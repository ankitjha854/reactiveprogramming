package com.reactivedemo.springreactivedemoapp.config;

import com.reactivedemo.springreactivedemoapp.dto.MultiplyRequestDto;
import com.reactivedemo.springreactivedemoapp.dto.Response;
import com.reactivedemo.springreactivedemoapp.service.ReactiveMathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
public class RequestHandler {

    @Autowired
    private ReactiveMathService mathService;


    public Mono<ServerResponse> sqHandler(ServerRequest serverRequest){
    int input =  Integer.parseInt( serverRequest.pathVariable("input"));
    Mono<Response> responseMono = this.mathService.findSquare(input);
    return ServerResponse
            .ok()
        .body(responseMono, Response.class);

    }

    public Mono<ServerResponse> tableHandler(ServerRequest serverRequest){
        int input =  Integer.parseInt( serverRequest.pathVariable("input"));
        Flux<Response> responseFlux = this.mathService.multipilcationTable (input);
        return ServerResponse.ok()
                .contentType(MediaType.TEXT_EVENT_STREAM)
                .body(responseFlux, Response.class);

    }
    public Mono<ServerResponse> multiplyHandler(ServerRequest serverRequest){
       Mono<MultiplyRequestDto> multiplyRequestDtoMono =
               serverRequest.bodyToMono(MultiplyRequestDto.class);
       Mono<Response> responseMono = this.mathService.multiply(multiplyRequestDtoMono);

        return ServerResponse.ok()
                .contentType(MediaType.TEXT_EVENT_STREAM)
                .body(responseMono, Response.class);

    }





}
