package com.reactivedemo.springreactivedemoapp.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class RouterConfig {

    @Autowired
private RequestHandler requestHandler;

    public RouterFunction<ServerResponse> highLevelRouter(){
        return RouterFunctions.route()
                .path("router" ,this::serverResponseRouterFunction)
                .build();
    }

    public RouterFunction<ServerResponse> serverResponseRouterFunction(){
        return RouterFunctions.route().GET("rsquare/{input}",
               requestHandler::sqHandler )
                .GET("table/{input}",
                        requestHandler::tableHandler )
                 .POST("multiply",
                requestHandler::multiplyHandler ).
                build();
    }



}
