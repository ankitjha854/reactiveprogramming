package com.reactivedemo.springreactivedemoapp.exceptionHandler;


import com.reactivedemo.springreactivedemoapp.InputFailedValidation.InputFailedValidationResponse;
import com.reactivedemo.springreactivedemoapp.exception.InputValidationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class InvalidExceptionHandler {


    @ExceptionHandler(InputValidationException.class)
    public ResponseEntity<InputFailedValidationResponse>
    handleException(InputValidationException ex){
        InputFailedValidationResponse response = new
                InputFailedValidationResponse();

        response.setErrorCode(ex.getErrorCode());
        response.setInput(ex.getInput());
        response.setMessage(ex.getMSG());

        return ResponseEntity.badRequest().body(response);

    }
}
