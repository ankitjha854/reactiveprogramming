package com.reactivedemo.springreactivedemoapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringreactivedemoappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringreactivedemoappApplication.class, args);

	}

}
