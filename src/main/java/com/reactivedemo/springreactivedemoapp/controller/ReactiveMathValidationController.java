package com.reactivedemo.springreactivedemoapp.controller;

import com.reactivedemo.springreactivedemoapp.dto.Response;
import com.reactivedemo.springreactivedemoapp.exception.InputValidationException;
import com.reactivedemo.springreactivedemoapp.service.ReactiveMathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;



@RestController
@RequestMapping("ReactiveMath")
public class ReactiveMathValidationController {
    @Autowired
    private ReactiveMathService mathService;

    @GetMapping("square/{input}/throw")
    public Mono<Response> findSquare(@PathVariable int input){
        if(input<10 || input >20)
            throw new InputValidationException(input);
        return this.mathService.findSquare(input);
    }

    @GetMapping("square/{input}/mono-throw")
    public Mono<Response> monoError(@PathVariable int input){
        return Mono.just(input)
                .handle((integer, sink) -> {
                    if(integer<10 || integer >20)
                        sink.next(integer);
                    else
                        sink.error(new InputValidationException(integer));

                } ).cast(Integer.class).flatMap(i -> this.mathService.findSquare(i));
    }

}
