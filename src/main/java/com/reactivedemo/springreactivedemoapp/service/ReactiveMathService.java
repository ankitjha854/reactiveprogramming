package com.reactivedemo.springreactivedemoapp.service;

import com.reactivedemo.springreactivedemoapp.dto.MultiplyRequestDto;
import com.reactivedemo.springreactivedemoapp.dto.Response;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ReactiveMathService {

    public Mono<Response> findSquare(int input) {
        return Mono.fromSupplier(
                () -> input * input
        ).map(Response::new);
    }

    public Flux<Response> multipilcationTable(int input) {
        return Flux.range(1,10)
                .doOnNext(i -> SleepUtil.sleepSeconds(1))
                .doOnNext(i -> System.out.println("processing " + i))
                .map( i -> new Response(i * input));
    }

    public Mono<Response> multiply(Mono<MultiplyRequestDto> dtoMono) {
        return dtoMono
                .map(dto -> dto.getFirst() * dto.getSecond())
                .map(Response::new);
    }




}
