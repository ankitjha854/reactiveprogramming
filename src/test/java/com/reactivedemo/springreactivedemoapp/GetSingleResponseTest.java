package com.reactivedemo.springreactivedemoapp;

import com.reactivedemo.springreactivedemoapp.dto.Response;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.reactive.function.client.WebClient;

public class GetSingleResponseTest extends BaseTest {



    @Autowired
    private WebClient webClient;

    @Test
    public void blockTest(){
      Response response
        = this.webClient
                .get()
                .uri("ReactiveMath/square/{input}", 5)
                .retrieve()
                .bodyToMono(Response.class)
                .block();
        System.out.println(response);
    }



}
