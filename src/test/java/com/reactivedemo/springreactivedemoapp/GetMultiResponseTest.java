package com.reactivedemo.springreactivedemoapp;

import com.reactivedemo.springreactivedemoapp.dto.Response;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class GetMultiResponseTest extends BaseTest{

    @Autowired
    private WebClient webClient;

    @Test
    public void fluxTestUsingStepVerifier(){
        Flux<Response> response
                = this.webClient
                .get()
                .uri("ReactiveMath/table/{input}", 5)
                .retrieve()
                .bodyToFlux(Response.class)
                        .doOnNext(System.out::println);

        StepVerifier.create(response).expectNextCount(10).verifyComplete();
    }



}
