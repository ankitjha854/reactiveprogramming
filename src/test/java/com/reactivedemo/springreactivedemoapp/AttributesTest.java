package com.reactivedemo.springreactivedemoapp;

import com.reactivedemo.springreactivedemoapp.dto.MultiplyRequestDto;
import com.reactivedemo.springreactivedemoapp.dto.Response;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class AttributesTest extends BaseTest{
    @Autowired
    private WebClient webClient;

    @Test
    public void postTest() {
        Mono<Response> res = this.webClient
                .post()
                .uri("ReactiveMath")
                .bodyValue(multiplyRequestDto(5, 2))
                .attribute("auth", "basic")
                .headers(h -> h.set("somekey", "someval"))
                .retrieve()
                .bodyToMono(Response.class)
                .doOnNext(System.out::println);

        StepVerifier.create(res)
                .expectNextCount(1)
                .verifyComplete();

    }

    public MultiplyRequestDto multiplyRequestDto(int a, int b) {
        MultiplyRequestDto multiplyRequestDto = new MultiplyRequestDto();
        multiplyRequestDto.setFirst(a);
        multiplyRequestDto.setSecond(b);
        return multiplyRequestDto;

    }
}